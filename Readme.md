Programmieraufgabe Android
-----------------------------

# Aufgabenstellung
Ziel ist es eine App zu entwickeln, die Mensen auf einer Karte darstellt und bei Auswahl einer Mensa
deren Menü für den aktuellen Tag angezeigt wird.

- Mensa als Marker auf Karte anzeigen
Dafür soll folgende API benutzt werden:
```
https://doc.openmensa.org/api/v2/canteens/
```
Es reicht, wenn eine einzige Mensa angezeigt wird bspw. die Mensa "Dresden, Alte Mensa" mit der Id 79.
Der Karten-Marker für die Mensa soll auf der Karte als Default Marker (Icon) dargestellt werden.

- Liste aller Namen der Gerichte vom heutigen Tag der ausgewählten Mensa anzeigen.
Dafür soll folgende API benutzt werden:
```
https://openmensa.org/api/v2/canteens/{id}/days/{day}/meals
```
Die Gerichte sollen in einem extra Fragment dargestellt werden. Es reicht, wenn die Namen der 
Gerichte in einem TextView angezeigt werden.

- Bonus: Alle Mensen im 25km Radius von Dresden auf der Karte anzeigen.